package com.Client;

import com.Table;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientRemoteInterface extends Remote {
        void updateClientGUI(Table table) throws RemoteException;
        String getName() throws RemoteException;
}
