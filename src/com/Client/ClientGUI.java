package com.Client;

import com.Server.ServerRemoteInterface;
import com.Table;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ClientGUI extends Application implements ClientRemoteInterface{
    private final int ROWS = 3;
    private final int COLUMNS = 3;
    private Button[][] buttons;
    private GridPane buttonGrid; //Contains the buttons matrix
    private Label lblTurn; //Shows player turn
    private ServerRemoteInterface remoteServer; //Remote object of server
    private String name; //Player name
    private static final StringBuilder turn = new StringBuilder();

    public static void main(String[] args) {
        launch(args);
    }

    //Show end game message.
    public static void prompt(String message){
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle("Game Ended");
        dialog.setHeaderText(null);
        dialog.setContentText(message);
        dialog.showAndWait();
    }

    @Override
    public void start(Stage primaryStage) {
        connect();
        GridPane root = new GridPane();
        root.setVgap(14);
        root.setHgap(14);

        buttonGrid = new GridPane();
        initiateGame();

        buttonGrid.setPrefSize(300, 300);
        buttonGrid.setHgap(8);
        buttonGrid.setVgap(8);
        lblTurn = new Label();
        lblTurn.setText(turn.toString() + "'s turn");

        root.add(lblTurn, 1, 1);
        root.add(buttonGrid, 1, 2);

        primaryStage.setTitle("Get 15: " + name);
        primaryStage.setScene(new Scene(root, 330,400));
        primaryStage.show();
    }

    private void connect()
    {
        try
        {
            System.out.println("Connecting...");
            Registry registry = LocateRegistry.getRegistry(1099);
            remoteServer = (ServerRemoteInterface) registry.lookup("Server");
            turn.append("Player1");
            int players =  remoteServer.getNextPlayer();
            name = "Player" + players;
            System.out.println(name);
            int port = 10000 + players; // Establish port to connect.
            UnicastRemoteObject.exportObject(this, port ); //Create remote client object for the server to use for updating client UI.
            remoteServer.registerPlayer(this);
            System.out.println("Connected to server.");
        }
        catch ( Exception e )
        {
            System.out.println("Connection failed due to: " + e);
            System.exit(1);
            return;
        }
    }

    public void initiateGame(){
        buttons = new Button[ROWS][COLUMNS];
        buttonGrid.getChildren().clear();
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                buttons[i][j] = new Button();
                buttons[i][j].setOnAction((e)->clicked(e)); //set ActionEvent for every buttons.
                buttonGrid.add(buttons[i][j],j+1, i+1);
                buttons[i][j].setPrefSize(100, 100);
                buttons[i][j].setStyle("-fx-font: 20 arial");
                try {
                    buttons[i][j].setText(remoteServer.gameState().getBoxChar(i,j));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //Event handler for buttons.
    private void clicked(ActionEvent e) {
        if(!name.equals(turn.toString())) return; //Check if it's this player's turn.
        for(int i= 0; i < ROWS; i++){
            for(int j= 0; j < COLUMNS; j++){
                if(e.getSource() == buttons[i][j]){ //Check which button the event came from.
                    try {
                        remoteServer.pick(i, j); //Pick a number.
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                    return;
                }
            }
        }
    }
    // Check if there is a winner.
    public void gameState(){
        Platform.runLater(()->{
            try {
                char winner = remoteServer.gameState().checkWinner();
                if(winner == ' ') { //Game is not over.
                    lblTurn.setText(turn.toString() + "'s turn");
                }
                else { //Game is over.
                    if (winner == '1') {
                        lblTurn.setText("Winner: Player1");
                        prompt("Winner: Player1");
                    }
                    else if (winner == '2') {
                        lblTurn.setText("Winner: Player2");
                        prompt("Winner: Player2");
                    }
                    else {
                        lblTurn.setText("It's a tie");
                        prompt("It's a tie.");
                    }
                    turn.replace(0, turn.length(), "Player1");
                    remoteServer.reset(); //Reset the game to play again.
                    initiateGame();
                }

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }
    //Update javafx components
    @Override
    public void updateClientGUI(Table table) throws RemoteException {
        Platform.runLater(()->{
            for(int i = 0; i < ROWS; i++){
                for(int j = 0; j < COLUMNS; j++){
                    if(!table.isAvailable(i,j)) { //Check if element has been picked.
                           //Set the next player to play their turn.
                           if(table.getPlayerTurn() == '2'){
                               turn.replace(0, turn.length(), "Player2");
                           }
                           else {
                               turn.replace(0, turn.length(), "Player1");
                           }
                           String style = String.format("-fx-background-color: %s; -fx-font-size: 20 arial", table.getBoxColor(i,j));
                           buttons[i][j].setStyle(style); //Set the color of the button.
                     }
                }
            }
        });
        gameState(); //Check if there is a winner.
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }

    @Override
    public void stop(){ //Stop the client app on closing the window.
        System.out.println("Client closing.");
        try {
            remoteServer.removePlayer(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
