package com;

import java.io.Serializable;
import java.util.ArrayList;

public class Table implements Serializable {
    private int[][] table; // Matrix with digits.
    private String[][] colors; //Matrix with colors for buttons.
    public static final int ROWS = 3;
    public static final int COLUMNS = 3;

    private int[] numbers; // Digits to initialize table with
    private char playerTurn;
    private ArrayList<Integer> playerOne; // Sums of digits drawn by Player1
    private ArrayList<Integer> playerTwo; // Sums of digits drawn by Player2
    private boolean haveWinner;

    public Table(){
        table = new int[ROWS][COLUMNS];
        colors = new String[ROWS][COLUMNS];
        resetTable();
    }

    public void resetTable(){
        haveWinner = false;
        playerTurn = '1';
        playerOne = new ArrayList<>();
        playerTwo = new ArrayList<>();
        numbers = new int[9];
        for(int i = 1; i < 10; i++) {
            numbers[i-1] = i;
        }
        int index = 0;
        for(int i =0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                colors[i][j] = "";
                table[i][j] = numbers[index]; //Set the value of table element
                index++;
            }
        }
    }
    //Pick an element in table
    public void pick(int row, int col){
        if(!haveWinner && colors[row][col].equals("")){

            if(playerTurn == '1'){
                colors[row][col] = "green";
                playerOne.add(table[row][col]);         //Add picked element
                playerTurn = '2';   //Set next player to play their turn
            }
            else {
                colors[row][col] = "red";
                playerTwo.add(table[row][col]);         //Add picked element
                playerTurn = '1';   //Set next player to play their turn
            }
        }
    }

    public char getPlayerTurn() {
        return playerTurn;
    }

    public String getBoxChar(int row, int col) { return "" + (char) ('0' + table[row][col]); }

    public String getBoxColor(int row, int col) { return colors[row][col]; }

    public boolean isAvailable(int row, int col) //Check if element has been picked by a player
    {
        return !haveWinner && colors[row][col].equals("");
    }

    //See if a player has won.
    public char checkWinner(){
        haveWinner = true;
        if(addEveryThree(playerOne)) return '1';
        if(addEveryThree(playerTwo)) return '2';
        if(playerOne.size() + playerTwo.size() == 9) return '3'; //If there is no winner but all digits have been taken it's a tie.
        haveWinner = false;
        return ' ';
    }

    public boolean addEveryThree(ArrayList<Integer> list){ //Checks if any three digits in a players list add up to 15.
        for(int i = 0; i < list.size() - 2; i++){
            for(int j = i + 1; j < list.size() - 1; j++) {
                for (int k = j + 1; k < list.size(); k++) {
                    int sum = list.get(i) + list.get(j) + list.get(k);
                    if(sum == 15) return true;
                }
            }
        }
        return false;
    }

}
