package com.Server;

import com.Client.ClientRemoteInterface;
import com.Table;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

public class ServerRemote extends UnicastRemoteObject implements ServerRemoteInterface{
    public final static int PORT = 1099;
    private Table numTable; //Represents game state.
    private Vector<ClientRemoteInterface> players = new Vector<>();
    private int nextPlayer; //Next, expected player to connect.

    private ServerRemote() throws RemoteException {
        super();
        nextPlayer = 1;
        numTable = new Table();
    }

    @Override
    public void pick(int row, int col) throws RemoteException {
        numTable.pick(row, col);
        updatePlayers();
    }

    private void updatePlayers() {
        for(int i = 0; i < players.size(); i++){
            try {
                players.get(i).updateClientGUI(numTable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void registerPlayer(ClientRemoteInterface player) throws RemoteException {
        players.add(player);
        nextPlayer = 2;
    }

    @Override
    public void reset() throws RemoteException {
        numTable.resetTable();
        updatePlayers();
    }

    @Override
    public Table gameState() throws RemoteException {
        return numTable;
    }

    @Override
    public void removePlayer(ClientRemoteInterface player) throws RemoteException {
        players.remove(player);
        if(player.getName().equals("Player1") || players.size() == 0) nextPlayer = 1; //Player1 expected to connect.
        else if(player.getName().equals("Player2")) nextPlayer = 2; //Player2 expected to connect.
    }
    //Get next player to register.
    @Override
    public int getNextPlayer() throws RemoteException {
        return nextPlayer;
    }

    public static void main(String[] args) {
        System.out.println("Initializing Server...");
        try
        {
            ServerRemote serverRemote = new ServerRemote();
            Registry registry = LocateRegistry.createRegistry(PORT);
            registry.rebind("Server", serverRemote);
            System.out.println("Server running.");
        }
        catch (Exception e)
        {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}
