package com.Server;

import com.Client.ClientRemoteInterface;
import com.Table;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerRemoteInterface extends Remote {
    void pick(int row, int col) throws RemoteException;

    void registerPlayer(ClientRemoteInterface player) throws RemoteException;

    void reset() throws RemoteException;

    Table gameState() throws  RemoteException;

    void removePlayer(ClientRemoteInterface player) throws RemoteException;

    int getNextPlayer() throws RemoteException;
}
